# Redis examples

Projects for exploring the awesomeness of [Redis].

## What is Redis
Redis is an "in-memory" key-value storage, with some interesting features.

* Lists
* Hashes
* Sets
* Sorted sets
* Pub / Sub
* Queing mechanism
* Atomic operations

See also [Redis data-types].

Redis is written i C (approx 20K lines of code) and can do 50.000 to 100.000 write operations/sec. Even 5X that speed if piping of operations are used. 

###Architecture

* All data lives in memory, and they are served insanely fast!
* The Redis codebase is kept simple in favor of speed. E.g. transactions are supported but not rollbacks.
* Data is persisted on disk in configurable intervals (RDB-files)
* All commands sent to Redis are appended to a file async
 (AOF)
* Single instance
* Master-slave replication. A second instance can serve as failover based on the RDB and AOF files(backups).

This means that the data sent to Redis is persisted async to file.

* Since it's in memory, the RDB file or/and AOF's are loaded into memory, which could take some time depending on the size of your data.
* This can be optimized with a combination using the RDB files and AOF's

### Installation

####The server
First off. The only thing you need to download is the Microsoft port of Redis from their Open Tech program.
This can be done via nuget packet manager.

```
install-package redis-32
```

or

```
install-package redis-64
```

>Then go to the "packages" folder in your solution and double-click "redis-server.exe". Thats all!! (at least for testing..)

>To start the Redis client, simply fire-up "redis-cli.exe". Type "ping" and you should get the response "pong" back.

We could benchmark the Redis instance on our machine with some built in performance-test functions

Open command-promt and navigate to "packages/redis-benchmark.exe"

```
 set lpush -n 100000
 10000 requests completed in 0.20 seconds
 50 parallel clients
 3 bytes payload
 keep alive: 1
 28.89%  less than 1 milliseconds
 93.29%  less than 2 milliseconds
 100.00% less than milliseconds
 50505.05 requests per second
```

####The .Net client
To use in your .Net project, install a client as well. We could use Booksleve, but since the author (Marc Gravell) of that library has created
a new and hopefully a better one, we'll use StackExchange.Redis

```
install-package StackExchange.Redis
```

For some simple examples, you can download this code and try it on your machine.

### Links
* [Redis commands]
* [StackExchange.Redis]

### Hope you'll give it a try!

Best regards, Terje Kvannli

License
----

MIT


**Free Software, Hell Yeah!**

[Redis]:http://redis.io/
[Redis commands]:http://redis.io/commands
[StackExchange.Redis]:https://github.com/StackExchange/StackExchange.Redis
[Redis data-types]:http://redis.io/topics/data-types
