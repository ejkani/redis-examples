﻿using System;
using System.Threading;
using RedisExample;
using StackExchange.Redis;

namespace RedisConsoleApp1
{
    class Program : HelperClass
    {
        static void Main(string[] args)
        {
            var redisConnection = ConnectionMultiplexer.Connect("127.0.0.1:6379");
            var db = redisConnection.GetDatabase();

            const string key1 = "test1";
            const string key2 = "test2";

            //Saving a string. Can be maximum 512 Mb....
            WriteCommand("SETEX", key1, "1 value1");
            db.StringSet(key1, "value1", TimeSpan.FromSeconds(1));

            WriteCommand("SET", key2, "value2");
            db.StringSet(key2, "value2");


            //Getting out the answer
            var answPreExpiey = db.StringGet(key1);
            WriteResult(String.Format("Value 1 BEFORE expiery timed out: '{0}'", answPreExpiey));
            
            //Make sure the value times out...
            Thread.Sleep(2000);
            
            //And try to get the answer again
            var result1 = db.StringGet(key1);
            var result2 = db.StringGet(key2);

            WriteResult(String.Format("Value 1 AFTER expiery timed out: '{0}'", result1));
            WriteResult(String.Format("Value 2 still exists: '{0}'", result2));

            Console.ReadLine();
        }
    }
}
