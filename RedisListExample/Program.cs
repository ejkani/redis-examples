﻿using System;
using RedisExample;
using StackExchange.Redis;

namespace RedisListExample
{
    class Program : HelperClass
    {
        static void Main(string[] args)
        {
            var redisConnection = ConnectionMultiplexer.Connect("127.0.0.1:6379");
            var db = redisConnection.GetDatabase();

            const string listKey = "list1";

            db.ListRemove(listKey, "A");
            db.ListRemove(listKey, "B");
            db.ListRemove(listKey, "C");

            //Add to list
            WriteCommand("LPUSH", listKey, "A");
            db.ListLeftPush(listKey, "A");

            WriteCommand("LPUSH", listKey, "B");
            db.ListLeftPush(listKey, "B");

            WriteCommand("RPUSH", listKey, "C");
            db.ListRightPush(listKey, "C");

            //Get data from list
            WriteCommand("LRANGE", listKey, "0 2");
            WriteResult(db.ListRange(listKey, 0, 2));

            Console.ReadLine();
        }
    }
}
