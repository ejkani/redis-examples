using System;
using System.Collections.Generic;
using StackExchange.Redis;

namespace RedisExample
{
    abstract class HelperClass
    {
        protected static void Write(string txt)
        {
            Console.ResetColor();
            Console.WriteLine(txt);
        }

        protected static void WriteCommand(string cmd, string key, string value)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("{0} {1} {2}", cmd, key, value);
            Console.WriteLine();
            Console.ResetColor();
        }

        protected static void WriteResult(IEnumerable<RedisValue> values)
        {
            foreach (var redisValue in values)
            {
                if(redisValue.HasValue)
                    WriteResult(redisValue);
            }
            Console.WriteLine();
        }

        protected static void WriteResult(string result)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-> \"{0}\"", result);
            Console.ResetColor();
        }
    }
}