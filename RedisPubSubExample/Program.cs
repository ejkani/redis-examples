﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedisExample;
using StackExchange.Redis;

namespace RedisPubSubExample
{
    class Program : HelperClass
    {
        static void Main(string[] args)
        {
            var redisConnection = ConnectionMultiplexer.Connect("127.0.0.1:6379");
            redisConnection.ConnectionFailed += (sender, e) => Write("Connection failed");

            Write("Setting up subscription first...");
            redisConnection.GetSubscriber().Subscribe("MyChannel", (channel, value) => WriteResult("Recieved value = " + value));

            Write("Then publishing a value...");
            redisConnection.GetSubscriber().Publish("MyChannel", "The_Published_Value");

            Console.ReadLine();
        }
    }
}
