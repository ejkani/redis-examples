﻿namespace RedisQueueExample
{
    public class ProcessAnswer
    {
        public string ProcessName { get; set; }
        public bool WasItOk { get; set; }
        public string Message { get; set; }
    }
}