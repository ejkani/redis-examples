﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace RedisQueueExample
{
    public static class Reactive
    {
        public static Subject<ProcessAnswer> ProcessAnswer { get; set; }

        static Reactive()
        {
            //We have to initialize the Reactive object before usage
            ProcessAnswer = new Subject<ProcessAnswer>();
        }
    }
}
