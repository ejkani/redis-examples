﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RedisExample;
using StackExchange.Redis;

namespace RedisQueueExample
{
    class Program : HelperClass
    {
        public static ConnectionMultiplexer RedisConnection = ConnectionMultiplexer.Connect("127.0.0.1:6379");
        public static IDatabase Db = RedisConnection.GetDatabase();

        static void Main(string[] args)
        {
            RedisConnection = ConnectionMultiplexer.Connect("127.0.0.1:6379");
            Db = RedisConnection.GetDatabase();

            SetUpSubscriptionsToWhatImInterestedIn();

            PushSomeDataIntoFirstQueue();

            var t1 = new Task(ProcessA.Execute);
            t1.Start();

            var t2 = new Task(ProcessB.Execute);
            t2.Start();

            var t3 = new Task(ProcessC.Execute);
            t3.Start();


            Console.ReadLine();
        }

        private static void SetUpSubscriptionsToWhatImInterestedIn()
        {
            //One way to create filtered subscriptions:
            var processASubscrQry = from x in Reactive.ProcessAnswer
                                    where x.ProcessName == "ProcessA"
                                    select x;

            processASubscrQry.Subscribe(OnProcessAIterationDone);

            //A more compact way to create subscriptions
            Reactive.ProcessAnswer.Where(x => x.ProcessName == "ProcessB").Subscribe(OnProcessBIterationDone);
            Reactive.ProcessAnswer.Where(x => x.ProcessName == "ProcessC").Subscribe(OnProcessCIterationDone);
        }

        #region Process subscription callbacks
        private static void OnProcessAIterationDone(ProcessAnswer answ)
        {
            ProcessA.Execute();
        }

        private static void OnProcessBIterationDone(ProcessAnswer answ)
        {
            ProcessB.Execute();
        }

        private static void OnProcessCIterationDone(ProcessAnswer answ)
        {
            ProcessC.Execute();
        }

        #endregion

        private static void PushSomeDataIntoFirstQueue()
        {
            foreach (var workInfo in CreateSomeQueueData())
            {
                Db.ListLeftPush("ProcessA", JsonConvert.SerializeObject(workInfo));
            }
        }

        private static IEnumerable<WorkInfo> CreateSomeQueueData()
        {
            return new List<WorkInfo>
            {
                new WorkInfo {Name = "Item1_"},
                new WorkInfo {Name = "Item2_"},
                new WorkInfo {Name = "Item3_"},
                new WorkInfo {Name = "Item4_"}
            };
        }
    }
}
