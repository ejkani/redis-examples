﻿using System;
using System.Threading;
using Newtonsoft.Json;

namespace RedisQueueExample
{
    public static class ProcessA
    {
        const string ThisProcessQueue = "ProcessA";
        const string ThisProcessing = "InProgressA";
        const string NextProcessQueue = "ProcessB";

        const ConsoleColor Color = ConsoleColor.DarkYellow;

        static ProcessA()
        {
            Console.WriteLine("Started {0} on thread '{1}'", ThisProcessQueue, Thread.CurrentThread.ManagedThreadId);
        }

        public static void Execute()
        {
            //Get the queue item by taking out the rightmost item from the first queue 
            //and inserting it to the left on the next queue. The item is returned in the same operation
            var orgItem = Program.Db.ListRightPopLeftPush(ThisProcessQueue, ThisProcessing);

            if (orgItem.IsNull)
            {
                //Ok. No items in queue. Wait a couple secondes before new iteration. 
                //To optimize throughput however, think about using Reactive Extensions
                //or Redis Pub/Sub (if its a distributed system) to notify when a new item is in the queue
                Thread.Sleep(2000);
                Reactive.ProcessAnswer.OnNext(NoMoreItemsInQueueAnsw());
                return;
            };

            var workItem = JsonConvert.DeserializeObject<WorkInfo>(orgItem);
            Console.ForegroundColor = Color;
            Console.WriteLine("{0}: Got work info for {1} ", ThisProcessQueue, workItem.Name);
            Console.ResetColor();

            //Do some heavy shit on work here....!
            Thread.Sleep(1000);

            //Work is done, now remove the item from the progress queue
            Program.Db.ListRemove(ThisProcessQueue, orgItem);

            //Put the item on the next queue
            var json = JsonConvert.SerializeObject(workItem);
            Program.Db.ListLeftPush(NextProcessQueue, json);

            //report back to whomever might be interested
            Reactive.ProcessAnswer.OnNext(ProcessingItemDoneAnsw());
        }

        private static ProcessAnswer ProcessingItemDoneAnsw()
        {
            return new ProcessAnswer
            {
                ProcessName = ThisProcessQueue,
                WasItOk = true,
                Message = ThisProcessQueue + " went goodie :-)"
            };
        }

        private static ProcessAnswer NoMoreItemsInQueueAnsw()
        {
            return new ProcessAnswer
            {
                ProcessName = ThisProcessQueue,
                WasItOk = true,
                Message = "No more items in queue for " + ThisProcessQueue
            };
        }
    }
}
